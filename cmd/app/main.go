package main

import (
	"log"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/app"
)

func main() {
	// Configuration
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	// Run
	app.Run(cfg)
}
