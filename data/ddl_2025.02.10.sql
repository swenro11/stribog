-- public.articles definition
-- DROP TABLE public.articles;
CREATE TABLE public.articles (
	id bigserial NOT NULL,
	title text NULL,
	slug text NULL,
	short_description text NULL,
	body text NULL,
	status text NULL,
	rewrite_notes text NULL,
	prompt text NULL,
	telegram_message_id int8 NULL,
	CONSTRAINT articles_pkey PRIMARY KEY (id)
);


-- public.images definition
-- DROP TABLE public.images;
CREATE TABLE public.images (
	id bigserial NOT NULL,
	alt_title text NULL,
	slug text NULL,
	sort int8 NULL,
	status text DEFAULT 'New'::text NULL,
	rewrite_notes text NULL,
	link text NULL,
	article_id int8 NULL,
	"base64" text NULL,
	"path" text NULL,
	prompt text NULL,
	CONSTRAINT images_pkey PRIMARY KEY (id)
);
-- public.images foreign keys
ALTER TABLE public.images ADD CONSTRAINT fk_articles_images FOREIGN KEY (article_id) REFERENCES public.articles(id);

-- public.projects definition
-- DROP TABLE public.projects;
CREATE TABLE public.projects (
	id bigserial NOT NULL,
	title text NULL,
	status text DEFAULT 'New'::text NULL,
	directory_path text NULL,
	description text NULL,
	url text NULL,
	CONSTRAINT projects_pkey PRIMARY KEY (id)
);

-- public.topics definition
-- DROP TABLE public.topics;
CREATE TABLE public.topics (
	id bigserial NOT NULL,
	title text NULL,
	status text DEFAULT 'New'::text NULL,
	project_id int8 NULL,
	parent_topic_id int8 NULL,
	CONSTRAINT topics_pkey PRIMARY KEY (id)
);
-- public.topics foreign keys
ALTER TABLE public.topics ADD CONSTRAINT fk_projects_topics FOREIGN KEY (project_id) REFERENCES public.projects(id);
ALTER TABLE public.topics ADD CONSTRAINT fk_topics_parent_topic FOREIGN KEY (parent_topic_id) REFERENCES public.topics(id);

-- public.keywords definition
-- DROP TABLE public.keywords;
CREATE TABLE public.keywords (
	id bigserial NOT NULL,
	title text NULL,
	slug text NULL,
	status text DEFAULT 'New'::text NULL,
	topic text NULL,
	topic_id int8 NULL,
	"source" text DEFAULT 'Unknown'::text NULL,
	CONSTRAINT keywords_pkey PRIMARY KEY (id)
);
-- public.keywords foreign keys
ALTER TABLE public.keywords ADD CONSTRAINT fk_topics_keywords FOREIGN KEY (topic_id) REFERENCES public.topics(id);

-- public.article_keywords definition
-- DROP TABLE public.article_keywords;
CREATE TABLE public.article_keywords (
	keyword_id int8 NOT NULL,
	article_id int8 NOT NULL,
	CONSTRAINT article_keywords_pkey PRIMARY KEY (keyword_id, article_id)
);
-- public.article_keywords foreign keys
ALTER TABLE public.article_keywords ADD CONSTRAINT fk_article_keywords_article FOREIGN KEY (article_id) REFERENCES public.articles(id);
ALTER TABLE public.article_keywords ADD CONSTRAINT fk_article_keywords_keyword FOREIGN KEY (keyword_id) REFERENCES public.keywords(id);

-- public.cities definition
-- DROP TABLE public.cities;
CREATE TABLE public.cities (
	status text DEFAULT 'New'::text NULL,
	id bigserial NOT NULL,
	wiki_data_id text NULL,
	city_type text NULL,
	"name" text NULL,
	country text NULL,
	country_code text NULL,
	region text NULL,
	region_code text NULL,
	latitude numeric NULL,
	longitude numeric NULL,
	region_wd_id text NULL,
	population int8 NULL,
	enwiki text NULL,
	endesc text NULL,
	CONSTRAINT cities_pk PRIMARY KEY (id)
);

-- public.coins definition
-- DROP TABLE public.coins;
CREATE TABLE public.coins (
	id text NOT NULL,
	symbol text NULL,
	"name" text NULL,
	image text NULL,
	current_price numeric NULL,
	market_cap int8 NULL,
	market_cap_rank int8 NULL,
	fully_diluted_valuation int8 NULL,
	total_volume int8 NULL,
	high_24h numeric NULL,
	low_24h numeric NULL,
	price_change_24h numeric NULL,
	price_change_percentage_24h numeric NULL,
	market_cap_change_24h numeric NULL,
	market_cap_change_percentage_24h numeric NULL,
	circulating_supply int8 NULL,
	total_supply int8 NULL,
	max_supply int8 NULL,
	ath numeric NULL,
	ath_change_percentage numeric NULL,
	ath_date timestamptz NULL,
	atl numeric NULL,
	atl_change_percentage numeric NULL,
	atl_date timestamptz NULL,
	roi jsonb NULL,
	last_updated timestamptz NULL,
	CONSTRAINT coins_pkey PRIMARY KEY (id)
);

-- public.countries definition
-- DROP TABLE public.countries;
CREATE TABLE public.countries (
	status text DEFAULT 'New'::text NULL,
	code text NULL,
	currency_codes text NULL,
	"name" text NULL,
	wiki_data_id text NULL,
	enwiki text NULL,
	endesc text NULL,
	CONSTRAINT countries_unique UNIQUE (code)
);

-- public.daos definition
-- DROP TABLE public.daos;
CREATE TABLE public.daos (
	id bigserial NOT NULL,
	message_cn text NULL,
	message_en text NULL,
	message_ru text NULL,
	status text DEFAULT 'New'::text NULL,
	"source" text DEFAULT 'Unknown'::text NULL,
	source_title text NULL,
	crawled_at timestamptz NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	CONSTRAINT daos_pkey PRIMARY KEY (id)
);

-- public.exchanges definition
-- DROP TABLE public.exchanges;
CREATE TABLE public.exchanges (
	id text NOT NULL,
	"name" text NULL,
	year_established int8 NULL,
	country text NULL,
	description text NULL,
	url text NULL,
	image text NULL,
	has_trading_incentive bool NULL,
	trust_score int8 NULL,
	trust_score_rank int8 NULL,
	trade_volume_24h_btc numeric NULL,
	trade_volume_24h_btc_normalized numeric NULL,
	CONSTRAINT exchanges_pkey PRIMARY KEY (id)
);

-- public.weathers definition
-- DROP TABLE public.weathers;
CREATE TABLE public.weathers (
	id bigserial NOT NULL,
	city_id int8 NULL,
	title text NULL,
	measure text NULL,
	january numeric NULL,
	february numeric NULL,
	march numeric NULL,
	april numeric NULL,
	may numeric NULL,
	june numeric NULL,
	july numeric NULL,
	august numeric NULL,
	september numeric NULL,
	october numeric NULL,
	november numeric NULL,
	december numeric NULL,
	updated_at timestamptz NULL,
	status text DEFAULT 'New'::text NULL,
	CONSTRAINT weathers_pkey PRIMARY KEY (id)
);