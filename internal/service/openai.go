package service

import (
	"context"
	"fmt"
	"os"

	"github.com/henomis/lingoose/llm/openai"
	"github.com/henomis/lingoose/thread"

	"gitlab.com/swenro11/stribog/config"
	log "gitlab.com/swenro11/stribog/pkg/logger"
)

type OpenAIService struct {
	cfg *config.Config
	log *log.Logger
}

func NewOpenAIService(cfg *config.Config, l *log.Logger) *OpenAIService {
	return &OpenAIService{
		cfg: cfg,
		log: l,
	}
}

// based on https://github.com/henomis/lingoose/blob/main/examples/llm/OpenAI/main.go
// need pay at least 5$ to use
func (service *OpenAIService) TextGenerationJson(inputPrompt string) (string, error) {
	os.Setenv("OPENAI_API_KEY", service.cfg.AI.OpenaiKey)
	openaillm := openai.New().WithModel(openai.GPT4o).WithResponseFormat(openai.ResponseFormatJSONObject).WithMaxTokens(1000)

	t := thread.New().AddMessage(
		thread.NewUserMessage().AddContent(
			thread.NewTextContent(inputPrompt),
		),
	)

	err := openaillm.Generate(context.Background(), t)
	if err != nil {
		return "", fmt.Errorf("OpenAIService.TextGenerationJson - llm.Chat: " + err.Error())
	}

	return t.String(), nil
}
