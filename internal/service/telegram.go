package service

import (
	"fmt"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/entity"
	log "gitlab.com/swenro11/stribog/pkg/logger"
)

type TelegramService struct {
	cfg *config.Config
	log *log.Logger
}

func NewTelegramService(cfg *config.Config, l *log.Logger) *TelegramService {
	return &TelegramService{
		cfg: cfg,
		log: l,
	}
}

func (service *TelegramService) SendMessage(article entity.Article) error {
	bot, err := tgbotapi.NewBotAPI(service.cfg.PARAM.TgBotApi)
	if err != nil {
		return fmt.Errorf("TelegramService.SendMessage - tgbotapi.NewBotAPI: " + err.Error())
	}
	intChatId, err := strconv.ParseInt(service.cfg.PARAM.TgChatId, 10, 64)
	if err != nil {
		return fmt.Errorf("TelegramService.SendMessage - strconv.ParseInt: " + err.Error())
	}
	msg := tgbotapi.NewMessage(intChatId, article.Title)

	resultMessage, err := bot.Send(msg)
	if err != nil {
		return fmt.Errorf("TelegramService.SendMessage - bot.Send: " + err.Error())
	}

	article.TelegramMessageId = &resultMessage.MessageID

	db, err := gorm.Open(postgres.Open(service.cfg.PG.URL), &gorm.Config{})
	if err != nil {
		service.log.Fatal("TelegramService.SendMessage - gorm.Open: %s", err)
	}

	db.Save(article)

	return nil
}
