package service

import (
	"encoding/json"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gocolly/colly"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/entity"
	log "gitlab.com/swenro11/stribog/pkg/logger"
)

const (
	CtextOrgUrl    = "ctext.org"
	BaseContentId  = "#content3"
	LastPartOfLink = "ens"
	HackerNewsUrl  = "https://news.ycombinator.com/item?id="
)

type CtextOrgCollyService struct {
	cfg         *config.Config
	log         *log.Logger
	comments    []*comment
	DaoMessages []entity.Dao
}

type comment struct {
	Author  string `selector:"a.hnuser"`
	URL     string `selector:".age a[href]" attr:"href"`
	Comment string `selector:".comment"`
	Replies []*comment
	depth   int
}

func NewCtextOrgCollyService(cfg *config.Config, l *log.Logger) *CtextOrgCollyService {
	return &CtextOrgCollyService{
		cfg: cfg,
		log: l,
	}
}

/*
- Parser colly https://github.com/gocolly
*/
func (service *CtextOrgCollyService) GetDaoMessages() {
	// Instantiate default collector
	c := colly.NewCollector(
		colly.AllowedDomains(CtextOrgUrl),
		colly.Async(true),
	)

	// On every a element which has "tbody tr" attribute call callback
	// This class is unique to the "tbody tr" that holds all DAO wisdoms/messages
	c.OnHTML(BaseContentId+" tbody tr", func(e *colly.HTMLElement) {
		//service.log.Info("tbody tr - " + e.Attr("href"))
		ctext := e.ChildText("td.ctext")

		source := e.Request.URL.String()
		sourceTitle := e.DOM.Find("h2").First().Text()

		//first tr with original text
		if ctext != "" {
			service.DaoMessages = append(service.DaoMessages, entity.Dao{
				MessageCn:   ctext,
				Source:      source,
				SourceTitle: &sourceTitle,
			})
		}

		//second tr with EN translate
		etext := e.ChildText("td.etext")
		if etext != "" {
			service.DaoMessages[len(service.DaoMessages)-1].MessageEn = &etext
		}
	})

	// On every span tag with the class next-button
	c.OnHTML("#content3 a", func(h *colly.HTMLElement) {
		href := h.Attr("href")
		//service.log.Info("find #content3 a - " + href)

		if len(href) > 3 {
			last3 := href[len(href)-3:]
			if last3 == LastPartOfLink {
				c.Visit(CtextOrgUrl + "/" + href)
			}
		}
	})

	// Set max Parallelism and introduce a Random Delay
	c.Limit(&colly.LimitRule{
		Parallelism: 2,
		RandomDelay: 5 * time.Second,
	})

	// Before making a request print "Visiting ..."
	c.OnRequest(func(r *colly.Request) {
		service.log.Info("Visiting %s", r.URL.String())
	})

	c.OnError(func(r *colly.Response, err error) {
		service.log.Error("Request URL: %s", r.Request.URL, "failed with response:", r, "\nError:", err)
	})

	// TODO: разобраться почему инфа только из первого URL сохранилась
	var baseUrls []string
	baseUrls = append(baseUrls, DaoDeJingURL)
	baseUrls = append(baseUrls, ZhuangziInnerChaptersURL)
	baseUrls = append(baseUrls, ZhuangziOuterChaptersURL)
	baseUrls = append(baseUrls, ZhuangziMiscellaneousChaptersURL)
	for _, url := range baseUrls {
		c.Visit(url)
	}

	c.Wait()
}

/*
- Based on https://github.com/gocolly/colly/blob/master/_examples/hackernews_comments/hackernews_comments.go
- Comments from HackerNewsUrl+40711284
- Title: Humans began to rapidly accumulate technological knowledge 600k years ago
*/
func (service *CtextOrgCollyService) ParseCommentsHackerNews() {
	var itemID string = "40711284"

	comments := make([]*comment, 0)

	// Instantiate default collector
	c := colly.NewCollector()

	// Extract comment
	c.OnHTML(".comment-tree tr.athing", func(e *colly.HTMLElement) {
		service.log.Info("find tr.athing" + e.Attr("id"))
		width, err := strconv.Atoi(e.ChildAttr("td.ind img", "width"))
		if err != nil {
			return
		}
		// hackernews uses 40px spacers to indent comment replies,
		// so we have to divide the width with it to get the depth
		// of the comment
		depth := width / 40
		c := &comment{
			Replies: make([]*comment, 0),
			depth:   depth,
		}
		e.Unmarshal(c)
		c.Comment = strings.TrimSpace(c.Comment[:len(c.Comment)-5])
		if depth == 0 {
			comments = append(comments, c)
			return
		}
		parent := comments[len(comments)-1]
		// append comment to its parent
		for i := 0; i < depth-1; i++ {
			parent = parent.Replies[len(parent.Replies)-1]
		}
		parent.Replies = append(parent.Replies, c)
	})

	c.Visit(HackerNewsUrl + itemID)

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "  ")

	// Dump json to the standard output
	enc.Encode(comments)

	service.comments = comments
}
