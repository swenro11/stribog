package service

import (
	"fmt"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/entity"
	log "gitlab.com/swenro11/stribog/pkg/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
// BaseURL              = "https://api-key.fusionbrain.ai/key/api/v1/"
)

type DaoService struct {
	cfg *config.Config
	log *log.Logger
	db  *gorm.DB
}

// example
type Dao struct {
	Uuid   string `json:"uuid"`
	Status string `json:"status"`
}

func NewDaoService(cfg *config.Config, l *log.Logger) *DaoService {
	db, err := gorm.Open(postgres.Open(cfg.PG.URL), &gorm.Config{})
	if err != nil {
		l.Fatal(err.Error())
	}

	return &DaoService{
		cfg: cfg,
		log: l,
		db:  db,
	}
}

func (service *DaoService) SaveDaoMessage(daoMessage entity.Dao) error {
	return service.Save(daoMessage.MessageCn, daoMessage.MessageEn, daoMessage.Source, daoMessage.SourceTitle)
}

func (service *DaoService) Save(ctext string, etext *string, source string, sourceTitle *string) error {
	err := service.db.Create(
		&entity.Dao{
			Status:      StatusNew,
			MessageCn:   ctext,
			MessageEn:   etext,
			Source:      source,
			SourceTitle: sourceTitle,
		})

	if err != nil {
		return err.Error
	}

	return nil
}

/*
-
*/
func (service *DaoService) FunctionWithServices() (*string, error) {
	// Dao

	keywordService := NewKeywordService(
		service.cfg,
		service.log,
	)

	db, err := gorm.Open(postgres.Open(service.cfg.PG.URL), &gorm.Config{})
	if err != nil {
		return nil, fmt.Errorf("gorm.Open error: %s", err)
	}

	var topics []entity.Topic
	db.Where(entity.Image{Status: StatusApproved}).Find(&topics)
	for _, topic := range topics {
		errCohereSaveKeywords := keywordService.CohereSaveKeywords(topic)
		if errCohereSaveKeywords != nil {
			return nil, fmt.Errorf("DaoService.New - errCohereSaveKeywords: " + errCohereSaveKeywords.Error())
		}

		errSaveKeyword := keywordService.OllamaSaveKeywords(KeywordsPrompt, topic)
		if errSaveKeyword != nil {
			return nil, fmt.Errorf("DaoService.New - errSaveKeyword: " + errSaveKeyword.Error())
		}
	}

	return nil, nil
}
