package service

import (
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/anaskhan96/soup"
	"github.com/geziyor/geziyor"
	"github.com/geziyor/geziyor/client"
	"github.com/geziyor/geziyor/export"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/entity"
	log "gitlab.com/swenro11/stribog/pkg/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type ParserService struct {
	cfg *config.Config
	log *log.Logger
}

// example
type Parser struct {
	Uuid   string `json:"uuid"`
	Status string `json:"status"`
}

func NewParserService(cfg *config.Config, l *log.Logger) *ParserService {
	return &ParserService{
		cfg: cfg,
		log: l,
	}
}

/*
- Parser soup - "github.com/anaskhan96/soup"
- Uncomfortable using. I prefer Jquery like sintax
*/
func (service *TasksService) TestParserSoup(cfg *config.Config) {
	id := 50
	url := fmt.Sprintf("https://xkcd.com/%d", id)
	xkcd, err := soup.Get(url)
	if err != nil {
		// Handle it
		service.log.Fatal(err.Error())
	}
	xkcdSoup := soup.HTMLParse(xkcd)

	links := xkcdSoup.Find("div", "id", "linkz")
	if links.Error != nil && links.Error.(soup.Error).Type == soup.ErrElementNotFound {
		service.log.Info("Element not found: %v", links.Error)
	}
	// These error types were introduced in version 1.2.0, but just checking for err still works:
	links = xkcdSoup.Find("div", "id", "links2")
	if links.Error != nil {
		service.log.Info("Something happened: %s", links.Error)
	}
}

/*
-
*/
func (service *ParserService) New() (*string, error) {
	// Mock

	keywordService := NewKeywordService(
		service.cfg,
		service.log,
	)

	db, err := gorm.Open(postgres.Open(service.cfg.PG.URL), &gorm.Config{})
	if err != nil {
		return nil, fmt.Errorf("gorm.Open error: %s", err)
	}

	var topics []entity.Topic
	db.Where(entity.Image{Status: StatusApproved}).Find(&topics)
	for _, topic := range topics {
		errCohereSaveKeywords := keywordService.CohereSaveKeywords(topic)
		if errCohereSaveKeywords != nil {
			return nil, fmt.Errorf("ParserService.New - errCohereSaveKeywords: " + errCohereSaveKeywords.Error())
		}

		errSaveKeyword := keywordService.OllamaSaveKeywords(KeywordsPrompt, topic)
		if errSaveKeyword != nil {
			return nil, fmt.Errorf("ParserService.New - errSaveKeyword: " + errSaveKeyword.Error())
		}
	}

	return nil, nil
}

/*
- Parser geziyor - https://github.com/geziyor/geziyor
*/
func (service *ParserService) ParseCtextOrgGeziyor() {
	geziyor.NewGeziyor(&geziyor.Options{
		StartURLs: []string{"https://ctext.org/dao-de-jing/ens"},
		ParseFunc: ctextOrgTableGeziyor,
		Exporters: []export.Exporter{&export.JSON{}},
	}).Start()
}

func ctextOrgTableGeziyor(g *geziyor.Geziyor, r *client.Response) {
	r.HTMLDoc.Find("t").Each(func(i int, s *goquery.Selection) {
		var sessions = strings.Split(s.Find(".shedule_session_time").Text(), " \n ")
		sessions = sessions[:len(sessions)-1]

		for i := 0; i < len(sessions); i++ {
			sessions[i] = strings.Trim(sessions[i], "\n ")
		}

		var description string

		if href, ok := s.Find("a.gtm-ec-list-item-movie").Attr("href"); ok {
			g.Get(r.JoinURL(href), func(_g *geziyor.Geziyor, _r *client.Response) {
				description = _r.HTMLDoc.Find("span.announce p.movie_card_description_inform").Text()

				description = strings.ReplaceAll(description, "\t", "")
				description = strings.ReplaceAll(description, "\n", "")
				description = strings.TrimSpace(description)

				g.Exports <- map[string]interface{}{
					"title":       strings.TrimSpace(s.Find("span.movie_card_header.title").Text()),
					"subtitle":    strings.TrimSpace(s.Find("span.sub_title.shedule_movie_text").Text()),
					"sessions":    sessions,
					"description": description,
				}
			})
		}

		/*
			s.Find("tbody", func(e *colly.HTMLElement) {
				t := make([]entity.Dao, 0)
				e.ForEach("tr", func(_ int, el *colly.HTMLElement) {
					etext := el.ChildText(".etext")
					t = append(t, entity.Dao{
						Status:    StatusNew,
						MessageCn: el.ChildText(".ctext"),
						MessageEn: &etext,
					})
				})
				cnText := e.Text
				service.log.Info("cnText found: %s\n", cnText)
			})

			c.OnHTML("td.etext", func(e *colly.HTMLElement) {
				enText := e.Text
				service.log.Info("enText found: %s\n", enText)
			})

			c.OnHTML("h2", func(e *colly.HTMLElement) {
				sourceTitle := e.Text
				service.log.Info("sourceTitle found: %s\n", sourceTitle)
			}*/
	})
	/*result := r.HTMLDoc.Find("tr").Each(func(_ int, s *goquery.Selection) {
		g.Exports <- map[string]interface{}{
			"cnText": s.Find("td.ctext").Text(),
			"enText": s.Find("td.etext").Text(),
		}
	})

	result.Each(func(_ int, selection *goquery.Selection) {
		enText := selection.Find("td.etext").Text()

		//pokemon = append(pokemon, sel.Find("h2").Text())
	})
	*/
}
