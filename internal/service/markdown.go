package service

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"

	"github.com/russross/blackfriday/v2"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/text"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/entity"
	log "gitlab.com/swenro11/stribog/pkg/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type MarkdownService struct {
	cfg *config.Config
	log *log.Logger
}

// example
type MarkdownPosition struct {
	Uuid   string `json:"uuid"`
	Status string `json:"status"`
}

func NewMarkdownService(cfg *config.Config, l *log.Logger) *MarkdownService {
	return &MarkdownService{
		cfg: cfg,
		log: l,
	}
}

/*
- Markdown Parser and HTML Renderer - https://github.com/gomarkdown/markdown
- work in progress
*/
func (service *MarkdownService) InsertTextToMdGomarkdown(inputText string, inputMd []byte, cfg *config.Config) ([]byte, error) {

	return nil, nil
}

/*
- Markdown parser - https://github.com/yuin/goldmark
- Need test.
*/
func (service *MarkdownService) InsertTextToMdGoldmark(inputText string, fileName string, cfg *config.Config) (*bytes.Buffer, error) {
	var buf bytes.Buffer

	inputMd, err := service.GetByteMd(fileName)
	if err != nil {
		return nil, fmt.Errorf("MarkdownService.InsertTextToMdGoldmark: %s", err)
	}
	/*
		if err := goldmark.Convert(inputMd, &buf); err != nil {
			return nil, fmt.Errorf("MarkdownService.InsertTextToMdGoldmark Convert: %s", err)
		}
		buf.
	*/

	md := goldmark.New(
		goldmark.WithExtensions(extension.GFM),
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(),
		),
	)

	if err := md.Convert(inputMd, &buf); err != nil {
		return nil, fmt.Errorf("MarkdownService.InsertTextToMdGoldmark Convert: %s", err)
	}

	var b bytes.Buffer

	doc := md.Parser().Parse(text.NewReader(inputMd))
	/*
	   doc.Meta()["footnote-prefix"] = getPrefix(path)
	*/
	errRender := md.Renderer().Render(&b, inputMd, doc)
	if errRender != nil {
		return nil, fmt.Errorf("MarkdownService.InsertTextToMdGoldmark Render: %s", errRender)
	}

	return &b, nil
}

func (service *MarkdownService) GetByteMd(fileName string) ([]byte, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	// Get the file size
	stat, err := file.Stat()
	if err != nil {
		return nil, err
	}

	// Read the file into a byte slice
	bs := make([]byte, stat.Size())
	_, err = bufio.NewReader(file).Read(bs)
	if err != nil && err != io.EOF {
		return nil, err
	}

	return bs, nil
}

/*
- Markdown processor - https://github.com/russross/blackfriday/tree/v2
- Need example
*/
func (service *MarkdownService) InsertTextToMdBlackfriday(inputText string, inputMd []byte, cfg *config.Config) ([]byte, error) {
	output := blackfriday.Run(inputMd)

	return output, nil
}

/*
-
*/
func (service *MarkdownService) New() (*string, error) {
	// Mock

	keywordService := NewKeywordService(
		service.cfg,
		service.log,
	)

	db, err := gorm.Open(postgres.Open(service.cfg.PG.URL), &gorm.Config{})
	if err != nil {
		return nil, fmt.Errorf("gorm.Open error: %s", err)
	}

	var topics []entity.Topic
	db.Where(entity.Image{Status: StatusApproved}).Find(&topics)
	for _, topic := range topics {
		errCohereSaveKeywords := keywordService.CohereSaveKeywords(topic)
		if errCohereSaveKeywords != nil {
			return nil, fmt.Errorf("MarkdownService.New - errCohereSaveKeywords: " + errCohereSaveKeywords.Error())
		}

		errSaveKeyword := keywordService.OllamaSaveKeywords(KeywordsPrompt, topic)
		if errSaveKeyword != nil {
			return nil, fmt.Errorf("MarkdownService.New - errSaveKeyword: " + errSaveKeyword.Error())
		}
	}

	return nil, nil
}
