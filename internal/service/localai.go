package service

import (
	"context"
	"fmt"

	"github.com/henomis/lingoose/llm/localai"
	"github.com/henomis/lingoose/thread"

	"gitlab.com/swenro11/stribog/config"
	log "gitlab.com/swenro11/stribog/pkg/logger"
)

const (
	/*
	   {"object":"list","data":[

	   	{"id":"gpt-3.5-turbo","object":"model"},
	   	{"id":"gpt4all-j","object":"model"},
	   	{"id":"gpt4all-snoozy-13b","object":"model"},
	   	{"id":"hermes-llama2-13b","object":"model"},
	   	{"id":"mistral-7b-openorca.Q4_0.gguf","object":"model"}

	   ]}
	*/
	LocalAIgpt3dot5turbo = "gpt-3.5-turbo"
)

type LocalAIService struct {
	cfg *config.Config
	log *log.Logger
}

func NewLocalAIService(cfg *config.Config, l *log.Logger) *LocalAIService {
	return &LocalAIService{
		cfg: cfg,
		log: l,
	}
}

// based on https://github.com/henomis/lingoose/blob/main/examples/llm/localai/main.go
func (service *LocalAIService) TextGenerationGpt3dot5turbo(inputPrompt string) (string, error) {
	localaillm := localai.New(service.cfg.AI.LocalAIURL).WithModel(LocalAIgpt3dot5turbo)
	t := thread.New().AddMessage(
		thread.NewUserMessage().AddContent(
			thread.NewTextContent(inputPrompt),
		),
	)

	err := localaillm.Generate(context.Background(), t)
	if err != nil {
		return "", fmt.Errorf("LocalAIService.TextGenerationGpt3dot5turbo - llm.Chat: " + err.Error())
	}

	return t.String(), nil
}
