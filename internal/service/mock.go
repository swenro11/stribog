package service

import (
	"fmt"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/entity"
	log "gitlab.com/swenro11/stribog/pkg/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
// BaseURL              = "https://api-key.fusionbrain.ai/key/api/v1/"
)

type MockService struct {
	cfg *config.Config
	log *log.Logger
}

// example
type Mock struct {
	Uuid   string `json:"uuid"`
	Status string `json:"status"`
}

func NewMockService(cfg *config.Config, l *log.Logger) *MockService {
	return &MockService{
		cfg: cfg,
		log: l,
	}
}

/*
-
*/
func (service *MockService) EmptyFunction() (*string, error) {
	// Mock

	return nil, nil
}

/*
-
*/
func (service *MockService) FunctionWithServices() (*string, error) {
	// Mock

	keywordService := NewKeywordService(
		service.cfg,
		service.log,
	)

	db, err := gorm.Open(postgres.Open(service.cfg.PG.URL), &gorm.Config{})
	if err != nil {
		return nil, fmt.Errorf("gorm.Open error: %s", err)
	}

	var topics []entity.Topic
	db.Where(entity.Image{Status: StatusApproved}).Find(&topics)
	for _, topic := range topics {
		errCohereSaveKeywords := keywordService.CohereSaveKeywords(topic)
		if errCohereSaveKeywords != nil {
			return nil, fmt.Errorf("MockService.New - errCohereSaveKeywords: " + errCohereSaveKeywords.Error())
		}

		errSaveKeyword := keywordService.OllamaSaveKeywords(KeywordsPrompt, topic)
		if errSaveKeyword != nil {
			return nil, fmt.Errorf("MockService.New - errSaveKeyword: " + errSaveKeyword.Error())
		}
	}

	return nil, nil
}
