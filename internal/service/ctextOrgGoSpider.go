package service

import (
	"fmt"

	"github.com/PuerkitoBio/goquery"
	"github.com/hu17889/go_spider/core/common/page"
	"github.com/hu17889/go_spider/core/pipeline"
	"github.com/hu17889/go_spider/core/spider"

	"gitlab.com/swenro11/stribog/config"
	"gitlab.com/swenro11/stribog/internal/entity"
	log "gitlab.com/swenro11/stribog/pkg/logger"
)

const (
	DaoDeJingURL                     = "https://ctext.org/dao-de-jing/ens"
	ZhuangziInnerChaptersURL         = "https://ctext.org/zhuangzi/inner-chapters/ens"
	ZhuangziOuterChaptersURL         = "https://ctext.org/zhuangzi/outer-chapters/ens"
	ZhuangziMiscellaneousChaptersURL = "https://ctext.org/zhuangzi/miscellaneous-chapters/ens"
)

type CtextOrgGoSpiderService struct {
	cfg         *config.Config
	log         *log.Logger
	DaoMessages []entity.Dao
}

func NewCtextOrgGoSpiderService(cfg *config.Config, l *log.Logger) *CtextOrgGoSpiderService {
	return &CtextOrgGoSpiderService{
		cfg: cfg,
		log: l,
	}
}

/*
- Parser go_spider - https://github.com/hu17889/go_spider
- Based on https://github.com/hu17889/go_spider/blob/master/example/github_repo_page_processor/main.go
*/
func (service *CtextOrgGoSpiderService) ParseCtextOrgGoSpider() {
	var baseUrls []string
	baseUrls = append(baseUrls, DaoDeJingURL)
	baseUrls = append(baseUrls, ZhuangziInnerChaptersURL)
	baseUrls = append(baseUrls, ZhuangziOuterChaptersURL)
	baseUrls = append(baseUrls, ZhuangziMiscellaneousChaptersURL)
	// Spider input:
	//  PageProcesser ;
	//  Task name used in Pipeline for record;
	spider.NewSpider(service, "CtextOrgGoSpider").
		AddUrls(baseUrls, "html").                  // Start url, html is the responce type ("html" or "json" or "jsonp" or "text")
		AddPipeline(pipeline.NewPipelineConsole()). // Print result on screen
		SetThreadnum(1).                            // Crawl request by three Coroutines
		Run()

	daoService := NewDaoService(
		service.cfg,
		service.log,
	)
	for _, daoMessage := range service.DaoMessages {
		daoService.SaveDaoMessage(daoMessage)
	}
}

// Parse html dom here and record the parse result that we want to Page.
// Package goquery (http://godoc.org/github.com/PuerkitoBio/goquery) is used to parse html.
func (service *CtextOrgGoSpiderService) Process(p *page.Page) {
	if !p.IsSucc() {
		println(p.Errormsg())
		return
	}

	query := p.GetHtmlParser()

	var urls []string
	query.Find("#content3 a").Each(func(i int, s *goquery.Selection) {
		if !s.HasClass("sprite-discuss") {
			href, _ := s.Attr("href")
			urls = append(urls, href)
		}
	})

	sourceTitle := query.Find("h2").First().Text()
	source := p.GetHtmlParser().Url.RequestURI()
	query.Find("tbody tr").Each(func(i int, s *goquery.Selection) {
		etext := query.Find("td.etext").Text()
		ctext := query.Find("td.ctext").Text()

		service.DaoMessages = append(service.DaoMessages, entity.Dao{
			MessageCn:   ctext,
			MessageEn:   &etext,
			Source:      source,
			SourceTitle: &sourceTitle,
		})
	})

	// these urls will be saved and crawed by other coroutines.
	p.AddTargetRequests(urls, "html")
}

func (service *CtextOrgGoSpiderService) Finish() {
	fmt.Printf("Count Messages %d \r\n", len(service.DaoMessages))
	fmt.Printf("End spider \r\n")
}
