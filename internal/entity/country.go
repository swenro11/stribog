package entity

type Country struct {
	Code          string `gorm:"primaryKey"`
	CurrencyCodes string
	Name          string
	WikiDataId    string
	Status        string `gorm:"default:New"`
	Enwiki        *string
	Endesc        *string
}
