package entity

import "time"

type Dao struct {
	ID          uint
	MessageCn   string
	MessageEn   *string
	MessageRu   *string
	Source      string `gorm:"default:Unknown"`
	SourceTitle *string
	Status      string    `gorm:"default:New"`
	CreatedAt   time.Time `gorm:"autoCreateTime:true"`
	UpdatedAt   time.Time
}
