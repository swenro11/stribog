package entity

type Project struct {
	ID             uint
	Title          string
	DirectoryPath  string
	Description    string
	Url            *string
	TelegramUrl    *string
	TelegramChatId *string
	Topics         []Topic
	Status         string `gorm:"default:New"`
}
