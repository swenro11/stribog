package entity

type Image struct {
	ID           uint
	AltTitle     *string
	Slug         *string
	ArticleID    uint
	Sort         *uint
	RewriteNotes *string
	Prompt       *string
	Link         *string
	Path         *string
	Base64       *string
	Status       string `gorm:"default:New"`
}
