package entity

type Exchange struct {
	Id                              string
	Name                            string
	Year_established                int
	Country                         string
	Description                     string
	Url                             string
	Image                           string
	Has_trading_incentive           bool
	Trust_score                     int
	Trust_score_rank                int
	Trade_volume_24h_btc            float32
	Trade_volume_24h_btc_normalized float32
}
