package entity

import (
	"time"

	"gorm.io/datatypes"
)

type Coin struct {
	Id                               string
	Symbol                           string
	Name                             string
	Image                            string
	Current_price                    float32
	Market_cap                       int64
	Market_cap_rank                  int64
	Fully_diluted_valuation          int64
	Total_volume                     int64
	High_24h                         float32
	Low_24h                          float32
	Price_change_24h                 float32
	Price_change_percentage_24h      float32
	Market_cap_change_24h            float32
	Market_cap_change_percentage_24h float32
	Circulating_supply               int64
	Total_supply                     int64
	Max_supply                       *int64
	Ath                              float32
	Ath_change_percentage            float32
	Ath_date                         time.Time
	Atl                              float32
	Atl_change_percentage            float32
	Atl_date                         time.Time
	Roi                              *datatypes.JSON
	Last_updated                     time.Time
}
