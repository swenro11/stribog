package entity

import (
	"time"

	"gorm.io/datatypes"
)

type N8nerror struct {
	Node      string
	Data      *datatypes.JSON
	CreatedAt time.Time `gorm:"autoCreateTime:true"`
}
