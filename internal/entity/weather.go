package entity

import "time"

type Weather struct {
	ID        uint
	CityId    uint
	Title     string
	Measure   string
	January   float64
	February  float64
	March     float64
	April     float64
	May       float64
	June      float64
	July      float64
	August    float64
	September float64
	October   float64
	November  float64
	December  float64
	UpdatedAt time.Time
	Status    string `gorm:"default:New"`
}
