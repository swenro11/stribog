package entity

type City struct {
	ID          uint
	WikiDataId  string
	CityType    string // type
	Name        string
	Country     string
	CountryCode string
	Region      string
	RegionCode  string
	RegionWdId  string
	Latitude    float64
	Longitude   float64
	Population  uint
	Status      string `gorm:"default:New"`
	Enwiki      *string
	Endesc      *string
}
